**Rust in Action**



<p class="bold"><a href="https://www.manning.com/books/rust-in-action?a_aid=rust&a_bid=0367c58f&chan=www"><em>Rust
    in
    Action</em></a> is a book for intermediate programmers who want to explore
the world of the Rust programming language. It's intended for people who may have exhausted the free material on
the web, but who still want to learn more.</p>

<p class="bold">Rust in Action is different from other material on Rust programming because it also teaches you
about systems programming along the way.
You'll be able to learn more about how a CPU works, how computers keep time, what pointers are and how your
network card and keyboard tell the CPU that they have input ready to read.
</p>

<p class="bold">It’s actually unique from the point of view of systems programming books too - as almost every
example works on Windows!</p>
<p class="bold">If you are the kind of learner who enjoys worked examples, you'll enjoy reading this book.</p>