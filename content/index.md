+++
title = "Welcome to Rust in Action"
date = 2019-05-20
+++


Welcome to [Rust in Action][], a book for intermediate programmers who are interested in the Rust programming
language. 


## Testimonials

> Yeah, I can vouch for this. It's really good! Especially the examples used. I'm amazed about the amount of thought that has gone in to them.
> &mdash; [/u/cfsamson](https://www.reddit.com/r/rust/comments/bqlznm/rust_pro_book/eo90d8s/?context=3)


  [Rust in Action]: https://www.manning.com/books/rust-in-action?a_aid=rust&a_bid=0367c58f&chan=www